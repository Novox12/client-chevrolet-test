import { Card, CardContent, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";

const Profile = ()=>{
    let navigate = useNavigate();

    const [profile, setProfile] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('infouser');
            if (dataExtra) {
                setProfile(dataExtra.data)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])


    return(
        <Card sx={{ minWidth: 275}}>
            <CardContent sx={{ textAlign: 'center'}}>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    Cedula: {profile.cedula}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    Nombre: {profile.name}
                    <br />
                    Apellido: {profile.apellido}
                    <br />
                    Email: {profile.email}
                </Typography>
            </CardContent>
        </Card>
    )
}

export default Profile