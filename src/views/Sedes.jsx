import {  Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";
import CrearSedes from "../components/crear/CrearSedes";
import EditarSede from "../components/editar/EditarSede";


const Sedes = () => {

    let navigate = useNavigate();

    const [sedes, setAreas] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('sedes');
            if (dataExtra) {
                setAreas(dataExtra.data.sedes)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    return (
        <>
            <h1>Sedes</h1>

            <CrearSedes />
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Sede</TableCell>
                            <TableCell align="left">direccion</TableCell>
                            <TableCell align="left">centro de costo</TableCell>
                            <TableCell align="left">Fecha de cración</TableCell>
                            <TableCell align="right">Acción</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sedes.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.nombre}</TableCell>
                                <TableCell align="left">{row.direccion}</TableCell>
                                <TableCell align="left">{row.centro_costo}</TableCell>
                                <TableCell align="left">{row.created_at}</TableCell>
                                <TableCell align="right">
                                    <EditarSede id={row.id} data={{nombre:row.nombre,direccion:row.direccion, centro_costo:row.centro_costo}} />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

           

        </>

    )
}

export default Sedes
