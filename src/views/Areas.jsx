import {  Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";
import CrearArea from "../components/crear/CrearArea";
import EditarArea from "../components/editar/EditarAreas";


const Areas = () => {

    let navigate = useNavigate();

    const [areas, setAreas] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('areas/all');
            if (dataExtra) {
                setAreas(dataExtra.data.areas)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    return (
        <>
            <h1>Areas</h1>
            <CrearArea/>
            
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Nombre del area</TableCell>
                            <TableCell align="left">Fecha de cración</TableCell>
                            <TableCell align="right">Acción</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {areas.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.nombre}</TableCell>
                                <TableCell align="left">{row.created_at}</TableCell>
                                <TableCell align="right">
                                    <EditarArea id={row.id} data={row.nombre} />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>

    )
}


export default Areas