import {  Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";
import EditarArea from "../components/editar/EditarAreas";
import CrearMarcas from "../components/crear/CrearMarcas";
import EditarMarca from "../components/editar/EditarMarca";


const Marcas = () => {

    let navigate = useNavigate();

    const [marcas, setAreas] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('marcas');
            if (dataExtra) {
                setAreas(dataExtra.data.marcas)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    return (
        <>
            <h1>Marcas</h1>
            <CrearMarcas />
            
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Marcas</TableCell>
                            <TableCell align="left">Fecha de cración</TableCell>
                            <TableCell align="right">Acción</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {marcas.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.marca}</TableCell>
                                <TableCell align="left">{row.created_at}</TableCell>
                                <TableCell align="right">
                                    <EditarMarca id={row.id} data={row.marca} />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>

    )
}


export default Marcas