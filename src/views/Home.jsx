import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { getById, getData } from '../services/api';
import {  LOGOUT } from '../config/routes/paths';
import { useNavigate } from "react-router-dom";
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

const bull = (
    <Box
        component="span"
        sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
        -
    </Box>
);


const Home = () => {
    let navigate = useNavigate();

    const [sedes, setSedes] = useState([])
    const [ordens, setOrdens] = useState([])
    const [age, setAge] = useState('');


    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('sedes');
            if (dataExtra?.data) {

                if (dataExtra.data.ok === true) {
                    setSedes(dataExtra.data.sedes)

                } else {
                    alert(dataExtra.data.msg)
                }

            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }

        extrarData()

    }, [])

    const handleChange = (event) => {
        setAge(event.target.value);
        const extrar = async () => {
            const dataEx = await getById('orden/sede', `${event.target.value}`)

            if (dataEx?.data) {
                if (dataEx.data.ok === true) {
                    setOrdens(dataEx.data.ordenes)

                } else {
                    alert(dataEx.data.msg)
                }
            } else {
                if (dataEx.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

        }
        extrar()
    };


    const handleData = () => {
        getById('', '')
    }

    return (
        <>
            <h1>Ordenes</h1>
            <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel id="demo-simple-select-label">Sedes</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={age}
                    label="Sedes"
                    onChange={handleChange}
                >
                    {
                        sedes.map((sede) => (
                            <MenuItem key={sede.id} value={sede.id}>{sede.nombre}</MenuItem>
                        ))
                    }
                </Select>
            </FormControl>            
            {
                ordens.map(orden => (
                    <Card key={orden.id} sx={{ minWidth: 275, marginBottom: 2 }}>
                        <CardContent>
                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                Id orden: {orden.id}
                            </Typography>
                            <Typography variant="h5" component="div">
                                Nombre: {orden.nombre}{bull}Apellido: {orden.apellido}{bull} Cedula: {orden.documento}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                Placa: {orden.placa}
                                <br />
                                Marca: {orden.marca}
                                <br />
                                Modelo: {orden.modelo}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                Fecha inicio: {orden.fecha_inicio}
                                <br />
                                Fecha final: {orden.fecha_fin}
                            </Typography>
                            <Typography variant="body2">
                                Observaciones:
                                <br />
                                {orden.observaciones}
                            </Typography>
                        </CardContent>
                    </Card>
                ))
            }


        </>
    );
}


export default Home