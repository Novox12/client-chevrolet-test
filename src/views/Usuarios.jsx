import { useEffect, useState } from "react"
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";
import CrearUsuarios from "../components/crear/CrearUsuario";
import EditarUsuarios from "../components/editar/EditarUsuarios";


const Usuarios = () => {
   
    let navigate = useNavigate();
    const [users, setUsers] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('user/all');
            if (dataExtra) {
                setUsers(dataExtra.data.users)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    return (
        <>
            <h1>Usuarios</h1>
            
            <CrearUsuarios />

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Nombre</TableCell>
                            <TableCell align="left">Apellido</TableCell>
                            <TableCell align="left">Cedula</TableCell>
                            <TableCell align="left">Email</TableCell>
                            <TableCell align="left">Area</TableCell>
                            <TableCell align="left">Rol</TableCell>
                            <TableCell align="left">Sede</TableCell>
                            <TableCell align="right">Acción</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.name}</TableCell>
                                <TableCell align="left">{row.apellido}</TableCell>
                                <TableCell align="left">{row.cedula}</TableCell>
                                <TableCell align="left">{row.email}</TableCell>
                                <TableCell align="left">{row.area_id}</TableCell>
                                <TableCell align="left">{row.role_id}</TableCell>
                                <TableCell align="left">{row.sede_id}</TableCell>
                                <TableCell align="right">
                                    <EditarUsuarios id={row.id} datos={{name:row.name, apellido:row.apellido, cedula:row.cedula, area_id:row.area_id, role_id:row.role_id, sede_id:row.sede_id}} />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>

    )
}

export default Usuarios
