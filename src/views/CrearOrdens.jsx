// Render Prop
import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, FormControl, InputLabel, MenuItem, Select, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { getData, postData, putData } from '../services/api';


const validationSchema = yup.object({

    documento:yup
    .number('Se requiere un número valido')
    .required("Requerido")
    .positive('Se requiere un número valido')
    .integer('Se requiere un número valido'),
    nombre:yup
    .string()
    .required('Campo requerido'),
    apellido:yup
    .string()
    .required('Campo requerido'),
    cedula:yup
    .number('Se requiere un número valido')
    .required("Requerido")
    .positive('Se requiere un número valido')
    .integer('Se requiere un número valido'),
    email:yup
    .string()
    .required('Campo requerido'),
    placa:yup
    .string()
    .required('Campo requerido'),
    vehiculo:yup
    .string()
    .required('Campo requerido'),
    marca:yup
    .string()
    .required('Campo requerido'),
    modelo:yup
    .string()
    .required('Campo requerido'),
    observaciones:yup
    .string()
    .required('Campo requerido'),
    fecha_inicio:yup
    .string()
    .required('Campo requerido'),
    fecha_fin:yup
    .string()
    .required('Campo requerido'),
    sede_id:yup
    .string()
    .required('Campo requerido'),
    user_id:yup
    .string()
    .required('Campo requerido')
});


const CrearOrdens = () => {

    const [areas,setArea] = useState([])
    const [marcas,setMarcas] = useState([])
    const [sedes, setSedes] = useState([])
    const [modelo, setModelo] = useState([])
    const [tecnicos, setTecnicos] = useState([])
    const [vehiculos, setVehiculos] = useState([])
    
    const [idSede, setIdSede] = useState(null)

    useEffect(() => {
        const extrarData = async () => {

            const dataExtraModelo = await getData('modelo/all');
            if (dataExtraModelo) {
                setModelo(dataExtraModelo.data.modelos)
            } else {
                if (dataExtraModelo.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

            const dataExtraMarca = await getData('marcas');
            if (dataExtraMarca) {
                setMarcas(dataExtraMarca.data.marcas)
            } else {
                if (dataExtraMarca.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

            const dataExtraArea = await getData('areas/all');
            if (dataExtraArea) {
                setArea(dataExtraArea.data.areas)
            } else {
                if (dataExtraArea.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

            const dataSedes = await getData('sedes');
            if (dataSedes) {
                setSedes(dataSedes.data.sedes)
            } else {
                if (dataSedes.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    const formik = useFormik({
        initialValues: {
            documento:'',
            nombre:'',
            apellido:'',
            cedula:'',
            email:'',
            placa:'',
            vehiculo:'',
            marca:'',
            modelo:'',
            observaciones:'',
            fecha_inicio:'',
            fecha_fin:'',
            sede_id:'',
            user_id:'',
            area:''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                
                const res = await postData(`orden/new`,{
                    documento:values.documento,
                    nombre:values.nombre,
                    apellido:values.apellido,
                    cedula:values.cedula,
                    email:values.email,
                    placa:values.placa,
                    vehiculo:values.vehiculo,
                    marca:values.marca,
                    modelo:values.modelo,
                    observaciones:values.observaciones,
                    fecha_inicio:values.fecha_inicio,
                    fecha_fin:values.fecha_fin,
                    sede_id:values.sede_id,
                    user_id:values.user_id
                })
                if (res.ok === true) {
                    alert('La orden fue creada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('La orden no fue creada, validar informacion')
                }
            }
            data()
        },
    });


    const extraTecnicos = async (idArea) => {

        if(idSede !== null){
            const dataExtraTecnicos = await getData(`tecnicos/area/${idSede}/${idArea}`);
            if (dataExtraTecnicos) {
                setTecnicos(dataExtraTecnicos.data.tecnicos)
            } else {
                if (dataExtraTecnicos.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }else{
            alert('Seleccione una sede antes de continuar')
        }


    }


    const calendarioTecnico =async(id_tec)=>{
        alert(id_tec)
    }

    const  vehiculoExtrac =async(id_marca)=>{
        const dataExtraTecnicos = await getData(`vehiculos/${id_marca}`);
        if (dataExtraTecnicos) {
            setVehiculos(dataExtraTecnicos.data.vehiculos)
        } else {
            if (dataExtraTecnicos.error === 'Request failed with status code 401') {
                navigate(LOGOUT);
                alert('Token inválido o sesión inspirada')
            }
            alert('Algo a fallado en su petición')
        }
    }



    return (
        <>

            <h1>Crear orden</h1>

            <form onSubmit={formik.handleSubmit}>
                <CardContent sx={{ width: '100%' }}>
                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-sede_id">Sede</InputLabel>
                        <Select
                            labelId="demo-simple-select-sede_id"
                            id="demo-simple-select"
                            value={formik.values.sede_id}
                            label="Sede"
                            name="sede_id"
                            onChange={formik.handleChange}
                            error={formik.touched.sede_id && Boolean(formik.errors.sede_id)}
                        >
                            {
                                sedes.map(sede=>(
                                    <MenuItem key={sede.id} onClick={()=>(setIdSede(sede.id))} value={sede.id}>{sede.nombre}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <h3>Información del cliente</h3>

                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="documento"
                        name="documento"
                        label="Documento"
                        value={formik.values.documento}
                        onChange={formik.handleChange}
                        error={formik.touched.documento && Boolean(formik.errors.documento)}
                        helperText={formik.touched.documento && formik.errors.documento}
                    /> 

                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="nombre"
                        name="nombre"
                        label="Nombre"
                        value={formik.values.nombre}
                        onChange={formik.handleChange}
                        error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                        helperText={formik.touched.nombre && formik.errors.nombre}
                    /> 

                    
                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="apellido"
                        name="apellido"
                        label="Apellido"
                        value={formik.values.apellido}
                        onChange={formik.handleChange}
                        error={formik.touched.apellido && Boolean(formik.errors.apellido)}
                        helperText={formik.touched.apellido && formik.errors.apellido}
                    />   


                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="cedula"
                        name="cedula"
                        label="Cedula"
                        value={formik.values.cedula}
                        onChange={formik.handleChange}
                        error={formik.touched.cedula && Boolean(formik.errors.cedula)}
                        helperText={formik.touched.cedula && formik.errors.cedula}
                    />   

                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="email"
                        name="email"
                        label="Email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />

                    <h3>Información del vehículo</h3>

                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="placa"
                        name="placa"
                        label="Placa"
                        value={formik.values.placa}
                        onChange={formik.handleChange}
                        error={formik.touched.placa && Boolean(formik.errors.placa)}
                        helperText={formik.touched.placa && formik.errors.placa}
                    />  

                 
                    
                    


                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-marca">Marca</InputLabel>
                        <Select
                            labelId="demo-simple-select-marca"
                            id="demo-simple-select"
                            value={formik.values.marca}
                            label="Marca"
                            name="marca"
                            onChange={formik.handleChange}
                            error={formik.touched.marca && Boolean(formik.errors.marca)}
                        >
                            {
                                marcas.map(row=>(
                                    <MenuItem key={row.id} onClick={()=>vehiculoExtrac(row.id)} value={row.marca}>{row.marca}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>

                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-vehiculo">Vehículo</InputLabel>
                        <Select
                            labelId="demo-simple-select-vehiculo"
                            id="demo-simple-select"
                            value={formik.values.vehiculo}
                            label="Vehículo"
                            name="vehiculo"
                            onChange={formik.handleChange}
                            error={formik.touched.vehiculo && Boolean(formik.errors.vehiculo)}
                        >
                            {
                                vehiculos.map(row=>(
                                    <MenuItem key={row.id} value={row.nombre}>{row.nombre}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>

                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-modelo">Modelo</InputLabel>
                        <Select
                            labelId="demo-simple-select-modelo"
                            id="demo-simple-select"
                            value={formik.values.modelo}
                            label="Modelo"
                            name="modelo"
                            onChange={formik.handleChange}
                            error={formik.touched.modelo && Boolean(formik.errors.modelo)}
                        >
                            {
                                modelo.map(row=>(
                                    <MenuItem key={row.id} value={row.modelo}>{row.modelo}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>

                    


                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        id="observaciones"
                        name="observaciones"
                        label="Observaciones del vehículo"
                        value={formik.values.observaciones}
                        onChange={formik.handleChange}
                        error={formik.touched.observaciones && Boolean(formik.errors.observaciones)}
                        helperText={formik.touched.observaciones && formik.errors.observaciones}
                    /> 
                    

                    <h3>Información de la orden</h3>


                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-area">Area</InputLabel>
                        <Select
                            labelId="demo-simple-select-area"
                            id="demo-simple-select"
                            value={formik.values.area}
                            label="Area"
                            name="area"
                            onChange={formik.handleChange}
                            error={formik.touched.area && Boolean(formik.errors.area)}
                            
                        >
                            {
                                areas.map(area=>(
                                    <MenuItem key={area.id} onClick={()=>extraTecnicos(area.id)} value={area.id}>{area.nombre}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>

                    <FormControl sx={{ marginBottom: 2 }} fullWidth>
                        <InputLabel id="demo-simple-select-user_id">Tecnico</InputLabel>
                        <Select
                            labelId="demo-simple-select-user_id"
                            id="demo-simple-select"
                            value={formik.values.user_id}
                            label="Tecnico"
                            name="user_id"
                            onChange={formik.handleChange}
                            error={formik.touched.user_id && Boolean(formik.errors.user_id)}
                            
                        >
                            {
                                tecnicos.map(tec=>(
                                    <MenuItem key={tec.id}  value={tec.id}>{tec.name} - {tec.apellido}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl> 


                    
                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        type='datetime-local'
                        id="fecha_inicio"
                        name="fecha_inicio"
                        label="Fecha Inicio"
                        value={formik.values.fecha_inicio}
                        onChange={formik.handleChange}
                        error={formik.touched.fecha_inicio && Boolean(formik.errors.fecha_inicio)}
                        helperText={formik.touched.fecha_inicio && formik.errors.fecha_inicio}
                    /> 

                    <TextField
                        sx={{ marginBottom: 2 }}
                        fullWidth
                        type='datetime-local'
                        id="fecha_fin"
                        name="fecha_fin"
                        label="Fecha fin"
                        value={formik.values.fecha_fin}
                        onChange={formik.handleChange}
                        error={formik.touched.fecha_fin && Boolean(formik.errors.fecha_fin)}
                        helperText={formik.touched.fecha_fin && formik.errors.fecha_fin}
                    /> 
                </CardContent>
                <CardActions>
                    <Button color="primary" variant="contained" fullWidth type="submit">
                        Crear Orden
                    </Button>

                </CardActions>
            </form>
        </>
    )
}

export default CrearOrdens;






