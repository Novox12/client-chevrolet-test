import { LOGOUT } from "../config/routes/paths"
import {Link} from 'react-router-dom'

const Private = () => {
    return (
        <>
            <h1>Private</h1>
            <Link to={LOGOUT} >Cerrar sesión</Link>
        </>
    )
}


export default Private