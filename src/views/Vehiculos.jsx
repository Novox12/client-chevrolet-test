import { useEffect, useState } from "react"
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { getData } from "../services/api"
import { useNavigate } from "react-router-dom";
import EditarVehiculos from "../components/editar/EditarVehiculos";
import CrearVehiculo from "../components/crear/CrearVehiculo";


const Vehiculos = () => {
   
    let navigate = useNavigate();
    const [vehiculos, setVehiculos] = useState([])

    useEffect(() => {
        const extrarData = async () => {
            const dataExtra = await getData('vehiculos/all');
            if (dataExtra) {
                setVehiculos(dataExtra.data.vehiculos)
            } else {
                if (dataExtra.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    return (
        <>
            <h1>Vehiculos</h1>
            
            <CrearVehiculo />

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Referencia</TableCell>
                            <TableCell align="left">Marca</TableCell>
                            <TableCell align="left">fecha de creación</TableCell>
                            <TableCell align="right">Acciones</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {vehiculos.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.nombre}</TableCell>
                                <TableCell align="left">{row.marca}</TableCell>
                                <TableCell align="left">{row.created_at}</TableCell>
                                <TableCell align="right">
                                    <EditarVehiculos id={row.id} datos={{nombre:row.nombre, marca_id:row.marca_id}} />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default Vehiculos
