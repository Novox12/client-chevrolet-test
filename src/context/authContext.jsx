import { createContext, useCallback, useContext, useMemo, useState } from "react"
import PropTypes from 'prop-types'
import { getAuth } from "../services/api"

const MY_AUTH_APP = 'MY_AUTH_APP'

export const AuthContext = createContext()

export const AuthContextProvider=({children}) =>{
    const [isAuthenticated, setIsAuthenticated] = useState(localStorage.getItem(MY_AUTH_APP) ?? false)

    const login = useCallback(function(){
        localStorage.setItem(MY_AUTH_APP, true)
        setIsAuthenticated(true)
    }, [])

    const logout = useCallback(function(){
        localStorage.removeItem(MY_AUTH_APP)
        localStorage.removeItem('accessToken')
        setIsAuthenticated(false)
    }, [])

    const value = useMemo(()=>({
        login,
        logout,
        isAuthenticated
    }), [login, logout, isAuthenticated])

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>

}

AuthContextProvider.protoType = {
    children: PropTypes.object
}


export const useAuthContext =()=>{
    return useContext(AuthContext)
}