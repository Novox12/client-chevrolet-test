import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { CONFIGURACION, CREARORDEN, LOGIN, LOGOUT, PRIVATE, PROFILE, USUARIOS, AREAS, SEDES, VEHICULOS, MARCAS } from './config/routes/paths'
import Login from './views/Login'
import Home from './views/Home'
import Logout from './views/Logout'
import Private from './views/Private'
import Profile from './views/Profile'
import { AuthContextProvider } from './context/authContext'
import PublicRoute from './components/router/PublicRoute'
import PrivateRoute from './components/router/PrivateRoute'
import Layout from './components/layout/Layout'
import Configuracion from './views/Configuracion'
import Areas from './views/Areas'
import Sedes from './views/Sedes'
import Usuarios from './views/Usuarios'
import Vehiculos from './views/Vehiculos'
import Marca from './views/Marca'
import CrearOrdens from './views/CrearOrdens'



const App = () => {
    return (
        <AuthContextProvider>
            <BrowserRouter>
                <Routes>

                    <Route path="/" element={<PublicRoute />}>
                        <Route path={LOGIN} element={<Login />} />
                    </Route>

                    <Route path={PRIVATE} element={<PrivateRoute />}>
                        <Route index element={<Layout><Home /></Layout>} />
                        <Route path={PRIVATE} element={<Private />} />
                        <Route path={PROFILE} element={<Layout> <Profile /> </Layout>} />
                        <Route path={CONFIGURACION} element={<Layout> <Configuracion /> </Layout>} />
                        <Route path={CREARORDEN} element={<Layout> <CrearOrdens /> </Layout>} />

                        <Route path={USUARIOS} element={<Layout> <Usuarios /> </Layout>} />
                        <Route path={AREAS} element={<Layout> <Areas /> </Layout>} />
                        <Route path={SEDES} element={<Layout> <Sedes /> </Layout>} />
                        <Route path={VEHICULOS} element={<Layout> <Vehiculos /> </Layout>} />
                        <Route path={MARCAS} element={<Layout> <Marca /> </Layout>} />

                        

                        <Route path={LOGOUT} element={<Logout />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </AuthContextProvider>
    )
}

export default App
