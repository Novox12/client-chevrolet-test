import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:8000/api/',
    mode: 'cors',
})

const token = () => {
    return localStorage.getItem('accessToken')
}

const config =()=> {
    return  { headers:{
            "Accept":"application/json",
            "Authorization":`Bearer ${token()}`
        }
    }
};

function getData(url) {
    return api.get(`/${url}`, config())
    .then(response  => {
        return response;
    }).catch(error => {
        return {message: 'ERROR', error : error.message };
    })
}

function getById(url, id) {
    return api.get(`/${url}/${id}`, config())
    .then(response => {
        return response;
    }).catch(error => {
        return error;
    })
}

function postData(url, data) {
    return api.post(`/${url}`, data, config())
    .then(response => {
        return response.data;
    }).catch(error => {
        return {'message':'Error','error':error};
    })
}

function patchData(url, data) {
    return api.patch(`/${url}`, data, config())
    .then(response => {
        return response.data;
    }).catch(error => {
        return {'message':'Error','error':error};
    })
}

function putData(url, data) {
    return api.put(`/${url}`, data, config())
    .then(response => {
        return response.data;
    }).catch(error => {
        return {'message':'Error','error':error};
    })
}


function getAuth(url, data) {
    return api.post(`/${url}`, data)
    .then(response  => {
        return response;
    }).catch(error => {
        return {'message':'Error','error':error};
    })
}

export { getAuth, getData, getById, postData, patchData, putData };
