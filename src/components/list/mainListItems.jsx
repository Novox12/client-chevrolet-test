import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import {Link} from 'react-router-dom'
import './style.css'
import { CREARORDEN, USUARIOS, AREAS, SEDES, VEHICULOS, MARCAS } from '../../config/routes/paths';

export const mainListItems = (
    <React.Fragment>
        
        <Link className='link-style' to={'/dashboard'} >
            <ListItemButton>
                <ListItemIcon>
                    <DashboardIcon />
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
            </ListItemButton>
        </Link>
        <Link className='link-style' to={CREARORDEN} >
            <ListItemButton>
                <ListItemIcon>
                    <LayersIcon />
                </ListItemIcon>
                <ListItemText primary="Crear orden" />
            </ListItemButton>
        </Link>
        <Link className='link-style' to={USUARIOS} >
            <ListItemButton>
                <ListItemIcon>
                    <PeopleIcon />
                </ListItemIcon>
                <ListItemText primary="Usuarios" />
            </ListItemButton>
        </Link>
        <Link className='link-style' to={AREAS} >
            <ListItemButton>
                <ListItemIcon>
                    <BarChartIcon />
                </ListItemIcon>
                <ListItemText primary="Areas" />
            </ListItemButton>
        </Link>
        <Link className='link-style' to={SEDES} >
            <ListItemButton>
                <ListItemIcon>
                    <LayersIcon />
                </ListItemIcon>
                <ListItemText primary="Sedes" />
            </ListItemButton>
        </Link>
       
        <Link className='link-style' to={MARCAS} >
            <ListItemButton>
                <ListItemIcon>
                    <LayersIcon />
                </ListItemIcon>
                <ListItemText primary="Marcas" />
            </ListItemButton>
        </Link>
        <Link className='link-style' to={VEHICULOS} >
            <ListItemButton>
                <ListItemIcon>
                    <LayersIcon />
                </ListItemIcon>
                <ListItemText primary="Vehículos" />
            </ListItemButton>
        </Link>
    </React.Fragment>
);
