// Render Prop
import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, FormControl, InputLabel, MenuItem, Select, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { getData, putData } from '../../services/api';



const validationSchema = yup.object({
    nombre:yup
        .string()
        .required('Campo requerido'),
    marca_id:yup
        .string()
        .required('Campo requerido')
});


const EditarVehiculos = ({id,datos}) => {

    const [marcas,setMarcas] = useState([])


    useEffect(() => {
        const extrarData = async () => {
        
            const dataExtracMarca = await getData('marcas');
            if (dataExtracMarca) {
                setMarcas(dataExtracMarca.data.marcas)
            } else {
                if (dataExtracMarca.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    const formik = useFormik({
        initialValues: {
            nombre:datos.nombre,
            marca_id:datos.marca_id
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await putData(`vehiculo/update/${id}`,{
                    nombre:values.nombre,
                    marca_id:values.marca_id
                })
                if (res) {
                    setOpen(false)
                    alert('El vehiculo fue Editado.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('El vehiculo no fue editado, validar informacion')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Editar Vehículo</Button>
            <SwipeableDrawer
                p={2}

                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Editar vehículo"></CardHeader>
                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="nombre"
                            name="nombre"
                            label="Nombre"
                            value={formik.values.nombre}
                            onChange={formik.handleChange}
                            error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                            helperText={formik.touched.nombre && formik.errors.nombre}
                        /> 

                        <FormControl sx={{ marginBottom: 2 }} fullWidth>
                            <InputLabel id="demo-simple-select-marca_id">Marca</InputLabel>
                            <Select
                                labelId="demo-simple-select-marca_id"
                                id="demo-simple-select"
                                value={formik.values.marca_id}
                                label="Marca"
                                name="marca_id"
                                onChange={formik.handleChange}
                                error={formik.touched.marca_id && Boolean(formik.errors.marca_id)}
                                
                            >
                                {
                                    marcas.map(marca=>(
                                        <MenuItem key={marca.id} value={marca.id}>{marca.marca}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                                        
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Editar Vehículo
                        </Button>

                    </CardActions>
                </form>

            

            </SwipeableDrawer>

        </>
    )
}

export default EditarVehiculos;






