// Render Prop
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import {  putData } from '../../services/api';



const validationSchema = yup.object({
    area: yup
        .string()
        .required('Campo requerido')
});


const EditarArea = ({id, data}) => {

    const formik = useFormik({
        initialValues: {
            area: data,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await putData(`areas/update/${id}`,{
                    nombre:values.area
                })
                if (res) {
                    setOpen(false)
                    alert('La area fue editada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('Algo a fallado en su petición')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Editar Area</Button>
            <SwipeableDrawer
                p={2}
                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Editar Area"></CardHeader>
                        <TextField
                            fullWidth
                            id="area"
                            name="area"
                            label="Area"
                            value={formik.values.area}
                            onChange={formik.handleChange}
                            error={formik.touched.area && Boolean(formik.errors.area)}
                            helperText={formik.touched.area && formik.errors.area}
                        />                        
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Editar Area
                        </Button>

                    </CardActions>
                </form>

            </SwipeableDrawer>

        </>
    )
}

export default EditarArea;



