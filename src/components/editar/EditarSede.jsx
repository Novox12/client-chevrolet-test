// Render Prop
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import {  putData } from '../../services/api';



const validationSchema = yup.object({
    nombre: yup
        .string()
        .required('Campo requerido'),
    direccion: yup
        .string()
        .required('Campo requerido'),
    centro_costo: yup
        .string()
        .required('Campo requerido')
});


const EditarSede = ({id, data}) => {

    const formik = useFormik({
        initialValues: {
            nombre: data.nombre,
            direccion: data.direccion,
            centro_costo:data.centro_costo
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await putData(`sedes/update/${id}`,{
                    nombre:values.nombre,
                    direccion:values.direccion,
                    centro_costo:values.centro_costo
                })
                if (res) {
                    setOpen(false)
                    alert('La sede fue editada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('Algo a fallado en su petición')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Editar Sede</Button>
            <SwipeableDrawer
                p={2}
                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Editar Sede"></CardHeader>
                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="nombre"
                            name="nombre"
                            label="Nombre"
                            value={formik.values.nombre}
                            onChange={formik.handleChange}
                            error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                            helperText={formik.touched.nombre && formik.errors.nombre}
                        />  
                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="direccion"
                            name="direccion"
                            label="Dirección"
                            value={formik.values.direccion}
                            onChange={formik.handleChange}
                            error={formik.touched.direccion && Boolean(formik.errors.direccion)}
                            helperText={formik.touched.direccion && formik.errors.direccion}
                        />  
                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="centro_costo"
                            name="centro_costo"
                            label="Centro de costo"
                            value={formik.values.centro_costo}
                            onChange={formik.handleChange}
                            error={formik.touched.centro_costo && Boolean(formik.errors.centro_costo)}
                            helperText={formik.touched.centro_costo && formik.errors.centro_costo}
                        />                      
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Editar Area
                        </Button>

                    </CardActions>
                </form>

            </SwipeableDrawer>

        </>
    )
}

export default EditarSede;



