// Render Prop
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { postData } from '../../services/api';



const validationSchema = yup.object({
    marca: yup
        .string()
        .required('Campo requerido')
});


const CrearMarcas = () => {

    const formik = useFormik({
        initialValues: {
            marca: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await postData('marcas/new',{
                    marca:values.marca
                })
                if (res) {
                    setOpen(false)
                    alert('La marca fue creada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('Algo a fallado en su petición')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Agregar Marca</Button>
            <SwipeableDrawer
                p={2}

                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Crear Marca"></CardHeader>
                        <TextField
                            fullWidth
                            id="marca"
                            name="marca"
                            label="Marca"
                            value={formik.values.marca}
                            onChange={formik.handleChange}
                            error={formik.touched.marca && Boolean(formik.errors.marca)}
                            helperText={formik.touched.marca && formik.errors.marca}
                        />                        
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Crear Marca
                        </Button>

                    </CardActions>
                </form>

            

            </SwipeableDrawer>

        </>
    )
}

export default CrearMarcas;



