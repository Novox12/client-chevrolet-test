// Render Prop
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { postData } from '../../services/api';



const validationSchema = yup.object({
    area: yup
        .string()
        .required('Campo requerido')
});


const CrearArea = () => {

    const formik = useFormik({
        initialValues: {
            area: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await postData('areas/new',{
                    nombre:values.area
                })
                if (res) {
                    setOpen(false)
                    alert('La area fue creada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('Algo a fallado en su petición')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Agregar Area</Button>
            <SwipeableDrawer
                p={2}

                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Crear Area"></CardHeader>
                        <TextField
                            fullWidth
                            id="area"
                            name="area"
                            label="Area"
                            value={formik.values.area}
                            onChange={formik.handleChange}
                            error={formik.touched.area && Boolean(formik.errors.area)}
                            helperText={formik.touched.area && formik.errors.area}
                        />                        
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Crear Area
                        </Button>

                    </CardActions>
                </form>

            

            </SwipeableDrawer>

        </>
    )
}

export default CrearArea;



