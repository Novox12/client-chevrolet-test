// Render Prop
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { postData } from '../../services/api';



const validationSchema = yup.object({
    sedes: yup
        .string()
        .required('Campo requerido'),
    direccion: yup
        .string()
        .required('Campo requerido'),
    centro_costo: yup
        .string()
        .required('Campo requerido')
});


const CrearSedes = () => {

    const formik = useFormik({
        initialValues: {
            sedes: '',
            direccion:'',
            centro_costo:''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await postData('sedes/new',{
                    nombre:values.sedes,
                    direccion:values.direccion,
                    centro_costo: values.centro_costo
                })
                if (res) {
                    setOpen(false)
                    alert('La sede fue creada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('Algo a fallado en su petición')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Agregar Sede</Button>
            <SwipeableDrawer
                p={2}

                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Crear sede"></CardHeader>
                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="sedes"
                            name="sedes"
                            label="Sede"
                            value={formik.values.sedes}
                            onChange={formik.handleChange}
                            error={formik.touched.sedes && Boolean(formik.errors.sedes)}
                            helperText={formik.touched.sedes && formik.errors.sedes}
                        />   

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="direccion"
                            name="direccion"
                            label="Dirección"
                            value={formik.values.direccion}
                            onChange={formik.handleChange}
                            error={formik.touched.direccion && Boolean(formik.errors.direccion)}
                            helperText={formik.touched.direccion && formik.errors.direccion}
                        />  

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="centro_costo"
                            name="centro_costo"
                            label="Centro de costo"
                            value={formik.values.centro_costo}
                            onChange={formik.handleChange}
                            error={formik.touched.centro_costo && Boolean(formik.errors.centro_costo)}
                            helperText={formik.touched.centro_costo && formik.errors.centro_costo}
                        />                       
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Crear Sede
                        </Button>

                    </CardActions>
                </form>

            

            </SwipeableDrawer>

        </>
    )
}

export default CrearSedes;



