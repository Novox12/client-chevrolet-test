// Render Prop
import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { Button, CardActions, CardContent, CardHeader, FormControl, InputLabel, MenuItem, Select, SwipeableDrawer, TextField } from '@mui/material';
import * as yup from 'yup';
import { getData, postData } from '../../services/api';



const validationSchema = yup.object({
    name:yup
        .string()
        .required('Campo requerido'),
    apellido:yup
        .string()
        .required('Campo requerido'),
    cedula:yup
        .number('Se requiere un número valido')
        .required("Requerido")
        .positive('Se requiere un número valido')
        .integer('Se requiere un número valido'),
    email:yup
        .string().email("Email invalido").required("Email requerido"),
    password:yup
        .string()
        .required("Contraseña requerida")
        .min(9, "Minimo 9 caracteres")
        .max(40, "Maximo 40 caracteres"),
    role_id:yup
        .string()
        .required('Campo requerido'),
    sede_id:yup
        .string()
        .required('Campo requerido'),
    area_id:yup
        .string()
        .required('Campo requerido')
});


const CrearUsuarios = () => {

    const [roles, setRoles] = useState([])
    const [areas,setArea] = useState([])
    const [sedes, setSedes] = useState([])

    useEffect(() => {
        const extrarData = async () => {
        
            const dataExtraRoles = await getData('roles');
            if (dataExtraRoles) {
                setRoles(dataExtraRoles.data.roles)
            } else {
                if (dataExtraRoles.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

            const dataExtraArea = await getData('areas/all');
            if (dataExtraArea) {
                setArea(dataExtraArea.data.areas)
            } else {
                if (dataExtraArea.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }

            const dataSedes = await getData('sedes');
            if (dataSedes) {
                setSedes(dataSedes.data.sedes)
            } else {
                if (dataSedes.error === 'Request failed with status code 401') {
                    navigate(LOGOUT);
                    alert('Token inválido o sesión inspirada')
                }
                alert('Algo a fallado en su petición')
            }
        }
        extrarData()
    }, [])

    const formik = useFormik({
        initialValues: {
            name:'',
            apellido:'',
            cedula:'',
            email:'',
            password:'',
            role_id:'',
            sede_id:'',
            area_id:''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const data = async()=>{
                const res = await postData('user/sede/new',{
                    name:values.name,
                    apellido:values.apellido,
                    cedula:values.cedula,
                    email:values.email,
                    password:values.password,
                    role_id:values.role_id,
                    sede_id:values.sede_id,
                    area_id:values.area_id
                })
                if (res) {
                    setOpen(false)
                    alert('La sede fue creada.')
                } else {
                    if (res.error === 'Request failed with status code 401') {
                        navigate(LOGOUT);
                        alert('Token inválido o sesión inspirada')
                    }
                    alert('El usuario no fue creado, validar informacion')
                }
            }
            data()
        },
    });

    const [open, setOpen] = useState(false)

    return (
        <>

            <Button sx={{ marginBottom: 2 }} variant="contained" color="primary" onClick={() => setOpen(true)}>Agregar Usuario</Button>
            <SwipeableDrawer
                p={2}

                anchor={'right'}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <div className="drawer-custom-2"></div>
                
                <form onSubmit={formik.handleSubmit}>
                    <CardContent sx={{ width: 450 }}>
                        <CardHeader title="Crear usuario"></CardHeader>
                        <FormControl sx={{ marginBottom: 2 }} fullWidth>
                            <InputLabel id="demo-simple-select-sede_id">Sede</InputLabel>
                            <Select
                                labelId="demo-simple-select-sede_id"
                                id="demo-simple-select"
                                value={formik.values.sede_id}
                                label="Sede"
                                name="sede_id"
                                onChange={formik.handleChange}
                                error={formik.touched.sede_id && Boolean(formik.errors.sede_id)}
                                
                            >
                                {
                                    sedes.map(sede=>(
                                        <MenuItem key={sede.id} value={sede.id}>{sede.nombre}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                        
                        <FormControl sx={{ marginBottom: 2 }} fullWidth>
                            <InputLabel id="demo-simple-select-role_id">Rol</InputLabel>
                            <Select
                                labelId="demo-simple-select-role_id"
                                id="demo-simple-select"
                                value={formik.values.role_id}
                                label="Rol"
                                name="role_id"
                                onChange={formik.handleChange}
                                error={formik.touched.role_id && Boolean(formik.errors.role_id)}
                                
                            >
                                {
                                    roles.map(rol=>(
                                        <MenuItem key={rol.id} value={rol.id}>{rol.nombre}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>


                        <FormControl sx={{ marginBottom: 2 }} fullWidth>
                            <InputLabel id="demo-simple-select-area_id">Area</InputLabel>
                            <Select
                                labelId="demo-simple-select-area_id"
                                id="demo-simple-select"
                                value={formik.values.area_id}
                                label="Area"
                                name="area_id"
                                onChange={formik.handleChange}
                                error={formik.touched.area_id && Boolean(formik.errors.area_id)}
                                
                            >
                                {
                                    areas.map(area=>(
                                        <MenuItem key={area.id} value={area.id}>{area.nombre}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                            {formik.touched.area_id && Boolean(formik.errors.area_id)}


                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="name"
                            name="name"
                            label="Nombre"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}
                        />   

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="apellido"
                            name="apellido"
                            label="Apellido"
                            value={formik.values.apellido}
                            onChange={formik.handleChange}
                            error={formik.touched.apellido && Boolean(formik.errors.apellido)}
                            helperText={formik.touched.apellido && formik.errors.apellido}
                        />   

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="cedula"
                            name="cedula"
                            label="Cedula"
                            value={formik.values.cedula}
                            onChange={formik.handleChange}
                            error={formik.touched.cedula && Boolean(formik.errors.cedula)}
                            helperText={formik.touched.cedula && formik.errors.cedula}
                        />  

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="email"
                            name="email"
                            label="Email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        /> 

                        <TextField
                            sx={{ marginBottom: 2 }}
                            fullWidth
                            id="password"
                            name="password"
                            label="Contraseña"
                            type="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                        />                  
                    </CardContent>
                    <CardActions>
                        <Button color="primary" variant="contained" fullWidth type="submit">
                            Crear usuario
                        </Button>

                    </CardActions>
                </form>

            

            </SwipeableDrawer>

        </>
    )
}

export default CrearUsuarios;






